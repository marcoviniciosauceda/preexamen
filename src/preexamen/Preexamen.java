/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**
 *
 * @author marco
 */
public class Preexamen {
    private int codigoVenta;
    private int tipo;
    private float precio;
    private float cantidad;
    
    
    public   Preexamen(){
        this.codigoVenta = 0;
        this.cantidad = 0.0f;
        this.precio = 0.0f;
        this.tipo = 0;
           
    }

    public Preexamen(int tipo, int codigoVenta, float precio, float cantidad) {
        this.tipo = tipo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.codigoVenta = codigoVenta;
    }
    public Preexamen(Preexamen otro) {
        this.tipo = this.tipo;
        this.precio = this.precio;
        this.codigoVenta = this.codigoVenta;
        this.cantidad = this.cantidad;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public float calcularCostoVenta(){
        float costo=0.0f;
        costo = this.cantidad*this.precio;
        return costo;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularCostoVenta() * 0.16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.calcularImpuesto() + this.calcularCostoVenta();
        return total;
    }
  
}
